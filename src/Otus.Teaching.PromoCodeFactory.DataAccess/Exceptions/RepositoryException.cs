﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Exceptions
{
    public  class RepositoryException : ApplicationException
    {
        public RepositoryException(string message) : base(message) { }
        public RepositoryException() : this(nameof(RepositoryException)) { }

    }
}
