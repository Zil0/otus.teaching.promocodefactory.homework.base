﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EFCore.Configurations.Administration
{
    public class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        private readonly static int EMAIL_LENGTH = 100;
        private readonly static int NAME_LENGTH = 50;
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.Property(x => x.Email).HasMaxLength(EMAIL_LENGTH);

            builder.Property(x => x.FirstName).HasMaxLength(NAME_LENGTH);
            builder.Property(x => x.LastName).HasMaxLength(NAME_LENGTH);
        }
    }
}
