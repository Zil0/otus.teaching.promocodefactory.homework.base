﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EFCore.Configurations.Administration
{
    public class RoleConfiguration : IEntityTypeConfiguration<Role>
    {
        private readonly static int NAME_LENGTH = 50;
        private readonly static int DESCRIPTION_LENGTH = 30;
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Role> builder)
        {
            //builder.Property(x => x.Name).HasMaxLength(NAME_LENGTH);
            builder.Property(x => x.Description)
                 // Не работает
                 .HasMaxLength(DESCRIPTION_LENGTH)
                 // Работает только через изменение типа
                 // https://stackoverflow.com/questions/54329859/entity-framework-core-incorrect-field-size
                 //.HasColumnType("varchar(100)");
                 ;
        }
    }
}




