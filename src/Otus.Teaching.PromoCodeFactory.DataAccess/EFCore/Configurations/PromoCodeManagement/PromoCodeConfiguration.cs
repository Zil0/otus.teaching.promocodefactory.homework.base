﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EFCore.Configurations.PromoCodeManagement
{
    public class PromoCodeConfiguration : IEntityTypeConfiguration<PromoCode>
    {
        private static readonly int NAME_LENGTH = 50;
        private static readonly int SERVICE_INFO_LENGTH = 100;
        private static readonly int CODE_LENGTH = 20;
        public void Configure(EntityTypeBuilder<PromoCode> builder)
        {
            builder.Property(x => x.PartnerName).HasMaxLength(NAME_LENGTH);
            builder.Property(x => x.Code).HasMaxLength(CODE_LENGTH);
            builder.Property(x => x.ServiceInfo).HasMaxLength(SERVICE_INFO_LENGTH);
        }
    }
}
