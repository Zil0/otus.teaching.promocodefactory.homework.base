﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EFCore.Configurations.PromoCodeManagement
{
    public class PreferenceConfiguration : IEntityTypeConfiguration<Preference>
    {
        private readonly static int NAME_LENGTH = 50;
        public void Configure(EntityTypeBuilder<Preference> builder)
        {
            builder.Property(x => x.Name).HasMaxLength(NAME_LENGTH);
        }
    }
}
