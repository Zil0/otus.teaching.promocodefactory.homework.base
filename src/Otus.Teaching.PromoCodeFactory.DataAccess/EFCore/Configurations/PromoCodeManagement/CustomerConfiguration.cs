﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EFCore.Configurations.PromoCodeManagement
{
    public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        private readonly static int EMAIL_LENGTH = 50;
        private readonly static int NAME_LENGTH = 50;
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.Property(x => x.Email).HasMaxLength(EMAIL_LENGTH);

            builder.Property(x => x.FirstName).HasMaxLength(NAME_LENGTH);
            builder.Property(x => x.LastName).HasMaxLength(NAME_LENGTH);
        }
    }
}
