﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EFCore.Configurations.PromoCodeManagement
{
    public class CustomerPreferenceConfiguration : IEntityTypeConfiguration<CustomerPreference>
    {

        public void Configure(EntityTypeBuilder<CustomerPreference> builder)
        {
            // Создаем principal key для CustomerPreference
            builder
                .HasKey(x => new { x.CustomerId, x.PreferenceId });

            // Создаем отношение many-to-many
            builder
                .HasOne(x => x.Preference)
                .WithMany(x => x.CustomerPreferences)
                .HasForeignKey(x => x.PreferenceId);

            builder
                .HasOne(x => x.Customer)
                .WithMany(x => x.CustomerPreferences)
                .HasForeignKey(x => x.CustomerId);
        }
    }
}
