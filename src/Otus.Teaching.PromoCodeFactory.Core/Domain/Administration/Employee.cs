﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName { get { return $"{FirstName} {LastName}"; } }

        public string Email { get; set; }

        /// <summary>
        /// Навигационное свойство Role
        /// </summary>
        public virtual Role Role { get; set; }

        /// <summary>
        /// Внешний ключ для навигационного свойства Role
        /// </summary>
        public Guid RoleId { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}