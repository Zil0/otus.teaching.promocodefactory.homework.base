﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseEntity
    {
        [StringLength(180, MinimumLength = 1)]
        [Column(TypeName = "VARCHAR(250)")]
        [Required]
        public string Name { get; set; }

        public string Description { get; set; }
    }
}