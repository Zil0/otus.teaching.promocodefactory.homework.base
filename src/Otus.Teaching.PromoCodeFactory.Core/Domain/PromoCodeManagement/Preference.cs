﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        public string Name { get; set; }

        /// <summary>
        /// Добавляем Reference navigation property CustomerPreferences
        /// </summary>
        public virtual ICollection<CustomerPreference> CustomerPreferences { get; set; }
    }
}