﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [Required(ErrorMessage = "Title is required.")]
        public string Email { get; set; }

        /// <summary>
        /// Добавляем Reference navigation property CustomerPreferences
        /// </summary>
        public virtual ICollection<CustomerPreference> CustomerPreferences { get; set; }

        /// <summary>
        /// One-to-many с Promocodes
        /// </summary>
        public virtual ICollection<PromoCode> PromoCodes { get; set; }
    }
}