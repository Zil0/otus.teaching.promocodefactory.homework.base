﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]/[action]")]
    public class RolesController : ControllerBase
    {
        private readonly IRepository<Role> _rolesRepository;

        public RolesController(IRepository<Role> rolesRepository)
        {
            _rolesRepository = rolesRepository;
        }
        
        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<RoleItemResponse>> GetRolesAsync()
        {
            var roles = await _rolesRepository.GetAllAsync();

            var rolesModelList = roles.Select(x =>
                new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList();

            return rolesModelList;
        }

        /// <summary>
        /// Удалить роль по id
        /// </summary>
        /// <returns>Возвращает остальных сотрудников</returns>
        [HttpPost]
        public async Task<StatusCodeResult> Delete([FromBody] BaseEntity entity)
        {
            if (entity == null)
            {
                return NotFound();
            }

            await _rolesRepository.DeleteAsync(entity.Id);

            return Ok();
        }

        /// <summary>
        /// Добавить роль
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<StatusCodeResult> Add([FromBody] Role role)
        {
            if (role == null)
            {
                return BadRequest();
            }

            await _rolesRepository.CreateAsync(role);

            return Ok();
        }

        /// <summary>
        /// Обновить роль
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<StatusCodeResult> Update([FromBody] Role role)
        {
            if (role == null)
            {
                return BadRequest();
            }

            await _rolesRepository.UpdateAsync(role);

            return Ok();
        }
    }
}