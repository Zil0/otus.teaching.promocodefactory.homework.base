﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class PromoCodeMapper
    {
        private static readonly int PROMOCODE_LIFETIME = 100;
        public static PromoCode MapFromModel(GivePromoCodeRequest request, Preference preference)
        {
            var date = DateTime.Now;

            var promoCode = new PromoCode()
            {
                Id = new Guid(),

                Code = request.PromoCode,
                PartnerName = request.PartnerName,
                ServiceInfo = request.ServiceInfo,

                BeginDate = date,
                EndDate = date.AddDays(PROMOCODE_LIFETIME),

                Preference = preference,
                PreferenceId = preference.Id
            };
            return promoCode;
        }
    }
}
